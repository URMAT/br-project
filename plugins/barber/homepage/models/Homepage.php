<?php namespace barber\Homepage\Models;

use Model;

/**
 * Model
 */
class Homepage extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'barber_homepage_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $jsonable = ['services_short_details', 'barber_short_details', 'social_icons_on_footer'];

    public $attachOne = [
        'logo' => 'System\Models\File',
    ];
}
