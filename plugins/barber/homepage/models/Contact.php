<?php namespace barber\Homepage\Models;

use Model;

/**
 * Model
 */
class Contact extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'barber_homepage_contact';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $jsonable = ['phone', 'days'];
}
