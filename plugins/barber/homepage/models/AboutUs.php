<?php namespace barber\Homepage\Models;

use Model;

/**
 * Model
 */
class AboutUs extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'barber_homepage_aboutus';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'image_one' => 'System\Models\File',
        'image_two' => 'System\Models\File'
    ];
}
