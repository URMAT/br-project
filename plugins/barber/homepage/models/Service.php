<?php namespace barber\Homepage\Models;

use Model;

/**
 * Model
 */
class Service extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'barber_homepage_services';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'service_image' => 'System\Models\File'
    ];
}
