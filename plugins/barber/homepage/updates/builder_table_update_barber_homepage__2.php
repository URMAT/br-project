<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepage2 extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->text('bla_bla')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->dropColumn('bla_bla');
        });
    }
}
