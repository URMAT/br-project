<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepage7 extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->string('social_title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->dropColumn('social_title');
        });
    }
}
