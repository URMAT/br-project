<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBarberHomepageBarbers extends Migration
{
    public function up()
    {
        Schema::create('barber_homepage_barbers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('about_barber');
            $table->string('position');
            $table->integer('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('barber_homepage_barbers');
    }
}
