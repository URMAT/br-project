<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBarberHomepagePrice extends Migration
{
    public function up()
    {
        Schema::create('barber_homepage_price', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->integer('price');
            $table->integer('sort');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('barber_homepage_price');
    }
}
