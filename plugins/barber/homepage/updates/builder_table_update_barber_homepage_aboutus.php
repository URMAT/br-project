<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepageAboutus extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_aboutus', function($table)
        {
            $table->string('title', 1024)->change();
            $table->string('working_time', 1024)->change();
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_aboutus', function($table)
        {
            $table->string('title', 191)->change();
            $table->string('working_time', 191)->change();
        });
    }
}
