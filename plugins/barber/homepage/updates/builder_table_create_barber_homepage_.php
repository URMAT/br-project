<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBarberHomepage extends Migration
{
    public function up()
    {
        Schema::create('barber_homepage_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('services_short_details');
            $table->text('barber_short_details');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('barber_homepage_');
    }
}
