<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepage4 extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->renameColumn('bla_bla', 'title');
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->renameColumn('title', 'bla_bla');
        });
    }
}
