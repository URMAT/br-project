<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBarberHomepageContact extends Migration
{
    public function up()
    {
        Schema::create('barber_homepage_contact', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('address')->nullable();
            $table->string('mail')->nullable();
            $table->string('phone')->nullable();
            $table->string('days')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('barber_homepage_contact');
    }
}
