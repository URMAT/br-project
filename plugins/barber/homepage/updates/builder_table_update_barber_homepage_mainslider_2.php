<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepageMainslider2 extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_mainslider', function($table)
        {
            $table->integer('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_mainslider', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
