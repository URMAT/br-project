<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBarberHomepageAboutus extends Migration
{
    public function up()
    {
        Schema::create('barber_homepage_aboutus', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->text('text');
            $table->string('working_time');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('barber_homepage_aboutus');
    }
}
