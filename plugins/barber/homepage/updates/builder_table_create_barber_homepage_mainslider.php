<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBarberHomepageMainslider extends Migration
{
    public function up()
    {
        Schema::create('barber_homepage_mainslider', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 1024);
            $table->string('description', 1024)->nullable();
            $table->string('url_img', 1024)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('barber_homepage_mainslider');
    }
}
