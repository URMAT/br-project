<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepage extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->text('barber_short_details')->nullable()->change();
            $table->text('services_short_details')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->text('barber_short_details')->nullable(false)->change();
            $table->text('services_short_details')->nullable(false)->change();
        });
    }
}
