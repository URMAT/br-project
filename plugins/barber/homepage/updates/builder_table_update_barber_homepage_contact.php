<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepageContact extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_contact', function($table)
        {
            $table->string('title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_contact', function($table)
        {
            $table->dropColumn('title');
        });
    }
}
