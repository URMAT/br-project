<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBarberHomepageServices extends Migration
{
    public function up()
    {
        Schema::create('barber_homepage_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('icon')->nullable();
            $table->integer('sort_order');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('barber_homepage_services');
    }
}
