<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepageMainslider extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_mainslider', function($table)
        {
            $table->dropColumn('url_img');
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_mainslider', function($table)
        {
            $table->string('url_img', 1024)->nullable();
        });
    }
}
