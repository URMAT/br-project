<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepagePrice extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_price', function($table)
        {
            $table->integer('sort_order')->nullable();
            $table->dropColumn('sort');
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_price', function($table)
        {
            $table->dropColumn('sort_order');
            $table->integer('sort');
        });
    }
}
