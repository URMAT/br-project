<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepageServices extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_services', function($table)
        {
            $table->integer('sort_order')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_services', function($table)
        {
            $table->integer('sort_order')->nullable(false)->change();
        });
    }
}
