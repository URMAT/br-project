<?php namespace barber\Homepage\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBarberHomepage3 extends Migration
{
    public function up()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->text('text_footer')->nullable();
            $table->text('social_icons_on_footer')->nullable();
            $table->text('social_links')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('barber_homepage_', function($table)
        {
            $table->dropColumn('text_footer');
            $table->dropColumn('social_icons_on_footer');
            $table->dropColumn('social_links');
        });
    }
}
